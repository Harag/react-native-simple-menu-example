import React, { Component } from 'react';
import { Text, View, ScrollView, TouchableOpacity, StyleSheet} from 'react-native';

import { DrawerNavigator, StackNavigator,NavigationActions } from 'react-navigation'

class Screen1 extends Component {
    render() { 
	return (
	    <View style={{alignItems: 'center',flex: 1, justifyContent: 'space-around'}}>
	      <Text>Drawer Item 1 Screen 1</Text>
	      <Text style={{margin: 10}} 
		    onPress={() => this.props.navigation.navigate('Screen2')}>
      Navigate To Drawer Item 1 Screen 2
	      </Text>
	    </View>
	);
    }
}

class Screen2 extends Component {
    render() { return <Text>Drawer Item 1 Screen2</Text> }
}

class Screen3 extends Component {
    render()
    {
	return(
	    <View style={{alignItems: 'center',flex: 1, justifyContent: 'space-around'}}>
	      <Text>Drawer Item 2 Screen 3</Text>
	      <Text style={{margin: 10}} 
		    onPress={() => this.props.navigation.navigate('Screen4')}>
		Navigate To Drawer Item 2 Screen 4
	      </Text>
	    </View> );
    }
}

class Screen4 extends Component {
    render() { return <Text>Drawer Item 2 Screen 4</Text> }
}

class SideMenu extends React.Component {
    
    navigateToScreen = (route) => () => {
	const navigateAction = NavigationActions.navigate({
	    routeName: route
	});
	this.props.navigation.navigate(navigateAction);
    }
    render() {
	
        return (<View style={styles.container}>
		<ScrollView>	
                <Text style={styles.navItemStyle} onPress={ this.navigateToScreen('Item1')}>
                Item1
		</Text>
                <View
		style={{
                    borderBottomColor: 'black',
                    borderBottomWidth: 1 }}/>
                <Text style={styles.navItemStyle} onPress={ this.navigateToScreen('Item2')}>
                Item2
		</Text>
		
		</ScrollView>
		</View>);
    }
}

const MyDrawer = DrawerNavigator(
    {
	Item1: {
	    screen: StackNavigator(
		{
		    Screen1: { screen: Screen1 },
		    Screen2: { screen: Screen2 }
		},
		{
		    navigationOptions: ({ navigation }) => ({			
			headerRight: <Text style={styles.menu} onPress={() => {navigation.navigate('DrawerOpen'); }}>Menu</Text>,
		    })
		}
	    )
	},
	Item2: {
	    screen: StackNavigator(
		{
		    Screen3: { screen: Screen3 },
		    Screen4: { screen: Screen4 }
		},
		{
		    navigationOptions: ({ navigation }) => ({			
			headerRight: <Text style={styles.menu} onPress={() => {navigation.navigate('DrawerOpen'); }}>Menu</Text>,
		    })
		})
	}
    },
    {
	drawerWidth: 300,
	contentComponent: SideMenu
    }
);

export default class App extends Component {
    render() {
	return (
	    <MyDrawer/>
	);
    }
}

const styles = StyleSheet.create({
    menu: {
	padding: 5
    },
    container: {
	paddingTop: 20,
	flex: 1
    },
    navItemStyle: {
	padding: 10
    },
    navSectionStyle: {
	backgroundColor: 'lightgrey'
    },
    sectionHeadingStyle: {
	paddingVertical: 10,
	paddingHorizontal: 5
    },
    footerContainer: {
	padding: 20,
	backgroundColor: 'lightgrey'
    }
});
